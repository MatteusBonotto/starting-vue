import Vue from 'vue'
import Router from 'vue-router'

import App from '../App.vue'

// importando os componentes (Counteudos)

let Form = () => import('@/components/HelloWorld')

Vue.use(Router)

export default new Router({
  routes: [
    { path: '*', name: 'Main', component: App },
    {
      path: '/',
      name: 'Main',
      component: App,
      children: [
        { path: '/form', name: 'Form', component: Form },
        // { path: '/form', name: 'Form', component: Form },
        // { path: '/form', name: 'Form', component: Form }
      ]
    }
  ]
})