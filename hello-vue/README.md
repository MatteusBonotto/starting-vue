# hello-vue

> Observação: Após ter instalado o NodeJS em sua máquina execute os comando abaixo para executar o projeto, lembrando que os comando estão em BASH (normal do Linux e Mac), se estiver em windows utilize um prompt em bash ou procure os comando para executar no prompt de comando normal

## Project setup 
Para instalar todos as libs do projeto execute este comando

```
npm install
```

### Compiles and hot-reloads for development
Para rodar o projeto em modo de desenvolvimento (local)

```
npm run serve
```

### Compiles and minifies for production
Para compilar o projeto em modo de produção, isto irá compilar o projeto na pasta "dist"

```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
